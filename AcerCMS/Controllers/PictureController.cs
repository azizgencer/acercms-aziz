﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;

namespace AcerCMS.Controllers
{
    public class PictureController : Controller
    {
        // GET: Picture
        public ActionResult Index()
        {
            var model = new List<Picture>();
            using (var db = new AcerCmsEntities())
            {
                model = db.Pictures.ToList();
            }
            return View(model);
        }
    }
}